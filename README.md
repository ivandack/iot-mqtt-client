# Entrega MQTT de IoT

En este código se encuentran:

* El programa productor de datos que simularía ser una mota (iot-producer.js)
* El programa consumidor que toma los datos y los guarda en InfluxDB (iot-consumer.js)

Para levantar todo el ambiente hay que correr:

```
docker-compose up
```

Esto levantará:

* influxdb
* mqtt
* grafana (con datasource y dashboard ya configurados)
* servicio consumidor
* servicio productor
