require('dotenv').config();
const mqtt = require('mqtt');

const MQTT_TOPIC = process.env.MQTT_BROKER_TOPIC;
const GENERATION_INTERVAL = parseInt(process.env.GEN_INTERVAL, 10);

const mqttClient  = mqtt.connect(process.env.MQTT_BROKER_HOST);

mqttClient.on('connect', function () {
  console.log('Conectado a MQTT');
  mqttClient.subscribe(MQTT_TOPIC, function (err) {
    if (!err) {
      setInterval(() => {
        const value = 5 + Math.floor(Math.random() * (30 - 1)) + 1;
        mqttClient.publish(MQTT_TOPIC, '' + value);
      }, GENERATION_INTERVAL);
    }
  })
})

mqttClient.on('message', function (topic, message) {
  console.log(`MQTT: Valor enviado: "${message}"`);
})
