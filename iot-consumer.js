require('dotenv').config();
const mqtt = require('mqtt');
const Influx = require('influx');

const MQTT_TOPIC = process.env.MQTT_BROKER_TOPIC;
const INFLUX_MEASUREMENT = process.env.INFLUXDB_MEASUREMENT;
const INFLUX_DATABASE = process.env.INFLUXDB_DATABASE;

const mqttClient = mqtt.connect(process.env.MQTT_BROKER_HOST);
const influx = new Influx.InfluxDB({
  host: process.env.INFLUXDB_HOST,
  database: INFLUX_DATABASE,
  schema: [{
    measurement: INFLUX_MEASUREMENT,
    tags: ['sender'],
    fields: {
      value: Influx.FieldType.INTEGER,
    },
  }, ],
});

async function writePoints(influx, tempValue) {
  try {
    await influx.writePoints([{
      measurement: INFLUX_MEASUREMENT,
      tags: {
        sender: 1,
      },
      fields: {
        value: tempValue,
      },
    }, ]);
  } catch (err) {
    console.error(err);
  }
}

// Crea la base de datos si no existe
async function checkDatabase() {
  await influx
    .getDatabaseNames()
    .catch(err => {
      console.error(`Error conectando a InfluxDB: ${err}`);
      process.exit(-1);
    })
    .then((databaseNames) => {
      if (!databaseNames.includes(INFLUX_DATABASE)) {
        influx.createDatabase(INFLUX_DATABASE);
      }
    })
    .catch(err => {
      console.error(`Error creando base de datos "${INFLUX_DATABASE}"!`);
      process.exit(-1);
    });
}

// Conecta y suscribe a MQTT
function startWithMQTT() {
  mqttClient.on('connect', function () {
    console.log('Conectado a MQTT');
    mqttClient.subscribe(MQTT_TOPIC, function (err) {
      if (!err) {
        console.log(`MQTT: Suscripto al tópico ${MQTT_TOPIC}`);
      }
    })
  })

  mqttClient.on('message', function (topic, message) {
    console.log(`MQTT: Valor recibido del tópico ${topic}: "${message}"`);
    writePoints(influx, parseInt(message, 10));
  })
}

checkDatabase();
startWithMQTT();